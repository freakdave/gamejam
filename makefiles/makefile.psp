TARGET ?= $(EXEC_DIR)/$(TARGET_EXEC)
PLATFORM = psp

BUILD_DIR ?= ./build/$(PLATFORM)
EXEC_DIR ?= ./release/$(PLATFORM)
SRC_DIRS ?= .
ROOT_DIR ?= ../..
INCLUDE_DIR ?= $(ROOT_DIR)/include
DEP_DIR ?= $(ROOT_DIR)/deps
LIB_DIR ?= $(ROOT_DIR)/lib

include $(ROOT_DIR)/Makefile.detect

REL_DIR = release
REL_OBJS = $(SRCS:%=$(BUILD_DIR)/$(REL_DIR)/%.o)

DBG_DIR = debug
DBG_OBJS = $(SRCS:%=$(BUILD_DIR)/$(DBG_DIR)/%.o)

IGNORE_FOLDERS = dreamcast windows linux null
FILTER_PLATFORMS = $(addprefix $(SRC_DIRS)/,$(addsuffix /$(WILDCARD),$(IGNORE_FOLDERS))) $(addprefix $(SRC_DIRS)/,$(IGNORE_FOLDERS))

SRCS := $(filter-out $(FILTER_PLATFORMS),$(subst ./,,$(call PATH_MK,$(call FIND_FILES,$(SRC_DIRS),*.c))))

INC_DEPS = $(SRC_DIRS) $(DEP_DIR)/stb/include $(DEP_DIR)/cglm/include  $(DEP_DIR)/libpspmath/include  $(DEP_DIR)/pthreads-emb-1.0/include $(DEP_DIR)/openal-psp/include
INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(call PATH_MK,$(call FIND_DIRS,$(SRC_DIRS)/))) $(call PATH_MK,$(call FIND_DIRS,$(INCLUDE_DIR)))
INC_FLAGS := $(addprefix -I,$(INC_DIRS)) $(addprefix -I,$(INC_DEPS))

BASE_CFLAGS =  -DPSP -Wall -Wextra -Wshadow -Wstack-protector -Wstrict-prototypes -Wformat=0  -std=gnu11 -fsingle-precision-constant -fdiagnostics-color
RELEASE_CFLAGS = $(BASE_CFLAGS) -DNDEBUG -g -Os -ffast-math -funsafe-math-optimizations -fomit-frame-pointer
DEBUG_CFLAGS = $(BASE_CFLAGS) -DDEBUG -O0 -g -ggdb -fno-inline -fno-omit-frame-pointer

# Use this when we release as a full iso to simplify
#ISO_FLAGS = -DPSP_ISO

#Windows users probably dont want this, but if you do, then compile all the libs mentioned
ifeq ($(OSFLAG),windows)
LOCAL_LIBS = 0
else
LOCAL_LIBS ?= 1
endif

ifeq ($(LOCAL_LIBS), 1)
LIB_PSPMATH = $(DEP_DIR)/libpspmath/libpspmath.a
LIB_PSPGL =  $(DEP_DIR)/pspgl/libGLU.a $(DEP_DIR)/pspgl/libGL.a
LIB_PTHREADS = $(DEP_DIR)/pthreads-emb-1.0/libpthread-psp.a
LIB_OPENAL = 
release: $(LIB_PSPMATH) $(LIB_PSPGL)
debug: $(LIB_PSPMATH) $(LIB_PSPGL)

LDFLAGS += -L$(DEP_DIR)/openal-psp/lib -L$(DEP_DIR)/pspgl
else
LIB_PSPMATH = -lpspmath
LIB_PSPGL = -lGLU -lGL
LIB_PTHREADS = -lpthread-psp
LIB_OPENAL = 
endif

LIBS    = $(LIB_DIR)/$(PLATFORM)/libgamejam.a $(LIB_PSPMATH) $(LIB_OPENAL) $(LIB_PTHREADS) $(LIB_PSPGL) -lpspaudiolib -lpspaudio -lm -lc -lpspvfpu -lpspgum -lpspgu -lpsprtc 
INCS	  = $(INC_FLAGS) -isystem $(DEP_DIR) $(DEP_INCS)

BUILD_PRX = 1
PSP_FW_VERSION = 500

PSP_EBOOT = $(EXEC_DIR)/EBOOT.PBP
PSP_EBOOT_DBG = $(EXEC_DIR)/EBOOTd.PBP
PSP_EBOOT_TITLE ?= $(TARGET) $(BUILDDATE)
PSP_EBOOT_SFO ?= $(BUILD_DIR)/PARAM.SFO
PSP_EBOOT_ICON ?= NULL
PSP_EBOOT_ICON1 ?= NULL
PSP_EBOOT_UNKPNG ?= NULL
PSP_EBOOT_PIC1 ?= NULL
PSP_EBOOT_SND0 ?= NULL
PSP_EBOOT_PSAR = NULL > /dev/null 2>&1

EXTRA_TARGETS   = $(PSP_EBOOT)

all: release

.PHONY: release
.PHONY: eboot_extras

release: OBJS = $(REL_OBJS)
release: TARGET = $(EXEC_DIR)/$(TARGET_EXEC)
debug: OBJS = $(DBG_OBJS)
debug: TARGET = $(EXEC_DIR)/$(TARGET_EXEC)d

PSPSDK = $(shell psp-config --pspsdk-path)
include $(ROOT_DIR)/makefiles/build.mak

release: assets $(PSP_EBOOT) 

debug: assets $(PSP_EBOOT_DBG)

$(BUILD_DIR)/$(REL_DIR)/%.c.o: %.c
	@$(call ECHO,> $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(CFLAGS) $(INC_FLAGS) $(RELEASE_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

$(BUILD_DIR)/$(DBG_DIR)/%.c.o: %.c
	@$(call ECHO,> $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(CFLAGS) $(INC_FLAGS) $(DEBUG_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

assets:
	-@$(call MKDIR_P,$(EXEC_DIR))
  ifneq ("$(wildcard assets/)","")
		-@$(call CP_R,assets/*,$(EXEC_DIR)/)
  endif
  ifneq ("$(wildcard assets_extra/eboot/)","")
		-@$(call CP_R,assets_extra/eboot/*,$(BUILD_DIR)/)
  endif

.PHONY: assets
.PHONY: clean
