/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\private.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src
 * Created Date: Wednesday, January 22nd 2020, 8:02:57 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef PRIVATE_H
#define PRIVATE_H

extern void Game_Main(int argc, char **argv);
extern void Game_Exit(void);
extern void Host_Update(float time);
extern void Host_Frame(float time);

extern void UI_Destroy(void);
#endif /* PRIVATE_H */
