TARGET_LIB ?= libgamejam
PLATFORM ?= linux

BUILD_DIR := ./build/$(PLATFORM)
SRC_DIRS := src
ROOT_DIR := .
INCLUDE_DIR := $(ROOT_DIR)/include
DEP_DIR := $(ROOT_DIR)/deps
LIB_DIR := $(ROOT_DIR)/lib

include $(ROOT_DIR)/Makefile.detect

REL_DIR = release
REL_LIB = lib/$(PLATFORM)/$(TARGET_LIB).a
REL_OBJS = $(SRCS:%=$(BUILD_DIR)/$(REL_DIR)/%.o)

DBG_DIR = debug
DBG_LIB = lib/$(PLATFORM)/$(TARGET_LIB)d.a
DBG_OBJS = $(SRCS:%=$(BUILD_DIR)/$(DBG_DIR)/%.o)

IGNORE_FOLDERS = psp dreamcast windows null
FILTER_PLATFORMS = $(addprefix $(SRC_DIRS)/, $(addsuffix /%, $(IGNORE_FOLDERS)))

INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -type d)) $(shell find $(INCLUDE_DIR) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS)) 

SRCS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -name "*.cpp" -or -name "*.c" -or -name "*.s"))
DEPS := $(REL_OBJS:.o=.d) $(DBG_OBJS:.o=.d)

BASE_CFLAGS = -Wall -Wextra -Wshadow -Wstack-protector -Wstrict-prototypes -Wformat=0 -std=gnu11 -fsingle-precision-constant -fdiagnostics-color
RELEASE_CFLAGS = $(BASE_CFLAGS) -DNDEBUG -g0 -Os -ffast-math -funsafe-math-optimizations -fomit-frame-pointer
DEBUG_CFLAGS = $(BASE_CFLAGS) -DDEBUG -g -O0 -fno-inline -fno-omit-frame-pointer

INCS = $(INC_FLAGS) -I $(INCLUDE_DIR)/common -isystem $(DEP_DIR) -isystem $(DEP_DIR)/glfw3/include -I$(DEP_DIR)/glad/include -I$(DEP_DIR)/stb/include -I$(DEP_DIR)/cglm/include -isystem $(DEP_DIR)/openal-win/include 

# Default builds release both
all:  release debug

release: $(REL_LIB)
debug: $(DBG_LIB)

$(REL_LIB): $(REL_OBJS)
	@echo -e "\n+ $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crs $@ $(REL_OBJS)

$(DBG_LIB): $(DBG_OBJS)
	@echo -e "\n+ $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crs $@ $(DBG_OBJS)

$(BUILD_DIR)/$(REL_DIR)/%.c.o: %.c
	@echo  "> $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(DEBUG_CFLAGS) $(INCS) $(LDFLAGS) -c -MM -MT $@ -MF $(patsubst %.o,%.d,$@) $<
	@$(CC) $(RELEASE_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

$(BUILD_DIR)/$(DBG_DIR)/%.c.o: %.c
	@echo  "> $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(DEBUG_CFLAGS) $(INCS) $(LDFLAGS) -c -MM -MT $@ -MF $(patsubst %.o,%.d,$@) $<
	@$(CC) $(DEBUG_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

examples: lib/$(PLATFORM)/$(TARGET_LIB)
	make -C examples

distclean: clean
	$(RM) -f lib/$(PLATFORM)/*

clean:
	$(RM) -rf $(BUILD_DIR)

-include $(DEPS)

.PHONY: clean
.PHONY: distclean
.PHONY: examples
