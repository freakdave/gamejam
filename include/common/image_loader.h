#pragma once
#ifndef IMAGE_LOADER_H
#define IMAGE_LOADER_H

#include "common.h"

/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\image_loader.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 9:17:14 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

typedef struct tx_image {
  char name[16];
  int width;
  int height;
  int channels;
  unsigned char *data;
  unsigned int id;
  unsigned int crc;
} tx_image;

typedef struct sprite {
  tx_image *parent;
  float u, v;
  float width, height;
  int i_width, i_height;
} sprite;

tx_image *IMG_load(const char *path);
tx_image *IMG_load_boolean(const char *path, bool transform);
tx_image *IMG_load_from_memory(const unsigned char *buffer, int len, bool _32bit);
void IMG_unload(tx_image *img);
void IMG_destroy(tx_image **img);

sprite IMG_create_sprite(tx_image *img, int x, int y, int x2, int y2);
sprite IMG_create_sprite_scaled(tx_image *img, int x, int y, int x2, int y2, float scale);

static inline sprite IMG_create_sprite_scaled_alt(tx_image *img, int x, int y, int w, int h, float scale) {
  return IMG_create_sprite(img, (int)(x * scale), (int)(y * scale), (int)((x + w) * scale), (int)((y + h) * scale));
}

static inline sprite IMG_create_sprite_alt(tx_image *img, int x, int y, int w, int h) {
  return IMG_create_sprite(img, x, y, x + w, y + h);
}

#endif /* IMAGE_LOADER_H */
