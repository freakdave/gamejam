/*
 * Filename: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/jfm/JFM_loader.h
 * Path: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/jfm
 * Created Date: Thursday, January 1st 1970, 12:00:00 am
 * Author: dev
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 * Copyright (c) 2006 Jake Stookey
 */

#pragma once

#include <common/common.h>

typedef struct model_jfm {
  float *vertices;          // vertices
  float *texturecoords;     // texturecoords
  int32_t *vertexindices;   // vertexindices
  int32_t *textureindices;  // textureindices

  int32_t num_vertex_indices;
  int32_t nv, num_texture_indices, nt, num_frames;

  int32_t currentframe;

  unsigned int texture;
  unsigned int crc;
} model_jfm;

model_jfm *JFM_load(const char *path);
model_jfm *JFM_load_boolean(const char *path, bool transform);
void JFM_destroy(model_jfm *obj);
void draw_jfm(model_jfm *jfm);
