#ifndef AABB_2D_H
#define AABB_2D_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\2d_physics\aabb_2d.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\2d_physics
 * Created Date: Wednesday, July 10th 2019, 2:11:02 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"
#include <cglm/cglm.h>
#include <cute_c2/cute_c2.h>

typedef struct AABB_2D
{
   vec2 min;
   vec2 max;
} AABB_2D;

AABB_2D AABB_2D_Create(vec2 min, vec2 max);
void AABB_2D_Translate(c2AABB *aabb, vec2 by);
bool AABB_2D_Intersects(AABB_2D *first, AABB_2D *second);

#endif /* AABB_2D_H */
