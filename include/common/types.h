#ifndef TYPES_H
#define TYPES_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\types.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Monday, July 8th 2019, 11:17:06 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
//CGLM Math
#include <cglm/cglm.h>

#ifdef PSP
#include <pspmath.h>
#define SIN(x) vfpu_sinf(x)
#define COS(x) vfpu_cosf(x)
#define SQRT(x) sqrtf(x)
#endif
#ifdef _arch_dreamcast
#include <dc/fmath.h>
#define SIN(x) fsin(x)
#define COS(x) fcos(x)
#define SQRT(x) fsqrt(x)
#endif
#if defined(WINDOWS) || defined(__linux)
#define SIN(x) sinf(x)
#define COS(x) cosf(x)
#define SQRT(x) sqrtf(x)
#endif

#endif /* TYPES_H */
