/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\voxels\voxel_main.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\voxels
 * Created Date: Wednesday, December 18th 2019, 6:15:37 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef VOXEL_MAIN_H
#define VOXEL_MAIN_H

#include <common.h>

extern scene voxel_main;

#endif /* VOXEL_MAIN_H */
