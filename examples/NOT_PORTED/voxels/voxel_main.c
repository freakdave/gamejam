/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\voxels\voxel_main.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\voxels
 * Created Date: Wednesday, December 18th 2019, 6:15:27 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "voxel_main.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "world/world.h"
#include "world/chunk.h"
#include "world/player.h"
#include "world/box.h"

static void Voxel_Init(void);
static void Voxel_Exit(void);
static void Voxel_Update(float time);
static void Voxel_Render2D(float time);
static void Voxel_Render3D(float time);
/* Registers the Title scene and populates the struct */
SCENE(voxel_main, &Voxel_Init, &Voxel_Exit, &Voxel_Render2D, &Voxel_Render3D, Voxel_Update, SCENE_BLOCK);
//STARTUP_SCENE(&Voxel_Init, NULL, &Voxel_Render2D, &Voxel_Render3D, Voxel_Update, SCENE_BLOCK);

static void Voxel_SetupChunk(void);
static void Voxel_PopulateChunk(chunk *_chunk);

/* floats for x rotation, y rotation, z rotation */
float xrot, yrot, zrot;
static chunk main_chunk;
static float angle = 0.0f;
//static float radius = 48.0f;
//static char dir = 0;

int player_CHNK_X;
int player_CHNK_Z;

tx_image *atlas;

static void Voxel_Init(void)
{
    extern void (*chunk_populate)(chunk *);
    chunk_populate = &Voxel_PopulateChunk;
    Voxel_SetupChunk();

    WRLD_Create(&main_chunk);
    PLYR_Create();
    atlas = IMG_load("tiles.png");
    RNDR_CreateTextureFromImage(atlas);

    player_CHNK_X = player_CHNK_Z = 0;
}

static void Voxel_Exit(void)
{
    PLYR_Destroy();
    resource_object_remove(atlas->crc);
}

static void Voxel_Update(float time)
{
    /* Update */
    angle += time;
    angle = fmod(angle, M_PI * 2);
    if (angle < 0.0f)
    {
        angle += ((float)M_PI * 2);
    }
    WLRD_Update(time);
    PLYR_Update();

    //#define CONV32(X) ((const union { float _f; uint32_t _u;}){ ._f = (X), }._u)
    if (INPT_DPADDirection(DPAD_LEFT_HELD))
    {
        angle -= 0.1f;
    }

    if (INPT_DPADDirection(DPAD_RIGHT_HELD))
    {
        angle += 0.1f;
    }
}

static void Voxel_Render2D(float time)
{
    //UI_DrawCharacter(320, 240, 'F');
    //UI_DrawTransSprite(640 - 256, 480 - 36, 256, 36, 1.0f, &logo_dreamon);
    //UI_DrawTransSprite(0, 0, logo_neodc.i_width, logo_neodc.i_height, 1.0f, &logo_neodc);

    //UI_DrawString(80, 80, "TUVWU");
    char mspf[32];
    //sprintf(mspf, "MSPF: %2.02f", (double)time * 1000);
    //UI_TextColor(1.0f, 0, 0);
    //UI_DrawString(80, 120, mspf);
    {
        float current_mspf = ((1000.0f / time) / 1000.0f);
        snprintf(mspf, 32, "%2.02f fps", current_mspf > 60.0f ? 60.0f : current_mspf);
    }
    UI_DrawString(80, 140, mspf);
    //sprintf(mspf, "Camera: %f %d", RAD2DEG(angle), (char)((angle + SX_CIRCLE) / (SX_CIRCLE)));
    //UI_DrawString(80, 160, mspf);

    snprintf(mspf, 32, "player {%3.2f, %3.2f} [%d, %d]", (double)player->aabb.base[0], (double)player->aabb.base[2], (int)player->aabb.base[0], (int)player->aabb.base[2]);
    UI_DrawString(80, 160, mspf);
}

static void Voxel_Render3D(float time)
{
    glPushMatrix();
    (void)time;
    // camera/view transformation
    mat4 view;
    glm_mat4_identity(view); // make sure to initialize matrix to identity matrix first

    //float camX = SIN(angle) * radius + radius / 2;
    //float camZ = COS(angle) * radius + radius / 2;
    /*glm_lookat((vec3){camX, 0.0f, camZ}, (vec3){0.0f, 0.0f, 0.0f}, (vec3){0.0f, 1.0f, 0.0f}, view);
    mat4 model, proj, mvp;
    vec4 viewport = {0.0f, 0.0f, 640.0f, 480.0f};*/
    //vec3 pos = {0.0f, 0.0f, 0.0f};
    //vec3 projected, unprojected;
    //gluLookAt(camX, 8, camZ, 16, 0, 16, 0.0f, 1.0f, 0.0f); //Current Camera

    gluLookAt(player->aabb.base[0], 32, player->aabb.base[2] + 24, player->aabb.base[0], 0, player->aabb.base[2], 0.0f, 0.0f, -1.0f); //Current Camera

    //gluLookAt(camX, radius, camZ, player->aabb.base[0], player->aabb.base[1], player->aabb.base[2], 0.0f, 1.0f, 0.0f);
    //gluLookAt(camX, 16, camZ, 16, 0, 16, 0.0f, 1.0f, 0.0f);

    //gluLookAt(player->aabb.base[0], 50, player->aabb.base[2], player->aabb.base[0], player->aabb.base[1], player->aabb.base[2], 0.0f, 1.0f, 0.0f);

    GL_Bind(atlas);

    int player_X = (int)player->aabb.base[0];
    int player_Z = (int)player->aabb.base[2];

    player_CHNK_X = player_X / 16;
    player_CHNK_Z = player_Z / 16;

#if 1
    glPushMatrix();
    glTranslatef((player_CHNK_X * CHNK_SIZE) - CHNK_SIZE, 0, (player_CHNK_Z * CHNK_SIZE) - CHNK_SIZE);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X - 1, 0, player_CHNK_Z - 1));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X, 0, player_CHNK_Z - 1));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X + 1, 0, player_CHNK_Z - 1));
    glPopMatrix();
#endif
#if 1
    glPushMatrix();
    glTranslatef((player_CHNK_X * CHNK_SIZE) - CHNK_SIZE, 0, (player_CHNK_Z * CHNK_SIZE));
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X - 1, 0, player_CHNK_Z + 0));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X + 0, 0, player_CHNK_Z + 0));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X + 1, 0, player_CHNK_Z + 0));
    glPopMatrix();
#endif
#if 1
    glPushMatrix();
    glTranslatef((player_CHNK_X * CHNK_SIZE) - CHNK_SIZE, 0, (player_CHNK_Z * CHNK_SIZE) + CHNK_SIZE);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X - 1, 0, player_CHNK_Z + 1));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X + 0, 0, player_CHNK_Z + 1));
    glTranslatef(CHNK_SIZE, 0, 0);
    WLRD_DrawChunk(WLRD_GetChunkAt(player_CHNK_X + 1, 0, player_CHNK_Z + 1));
    glPopMatrix();
#endif
    PLYR_Draw();
    glPopMatrix();
}

static void Voxel_SetupChunk(void)
{
    main_chunk = CHNK_create(0, 0);
    Voxel_PopulateChunk(&main_chunk);
}

static void Voxel_PopulateChunk(chunk *_chunk)
{
    for (int x = 0; x < CX; x++)
    {
        for (int y = 0; y < CY; y++)
        {
            for (int z = 0; z < CZ; z++)
            {

                if (_chunk->worldX + _chunk->worldZ != 0)
                {
                    char type = 0;
                    if (y == 0)
                    {
                        type = 1;
                    }

                    if (type)
                    {
                        CHNK_set(_chunk, x, y, z, type);
                    }
                }
                else
                {
                    char type = 1;
                    if (y > ((2 * SIN(x + (_chunk->worldX * 16))) + (2 * SIN(z + (_chunk->worldZ * 16)))))
                    {
                        type = 0;
                    }
                    /* Wall in the middle */
                    if ((x == 8) && y < 8)
                    {
                        //type = 1;
                    }

                    if (y == 0)
                    {
                        type = 1;
                    }

                    if (type)
                    {
                        CHNK_set(_chunk, x, y, z, type);
                    }
                }
            }
        }
    }

    CHNK_update(_chunk);
}
