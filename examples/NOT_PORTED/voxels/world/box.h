#ifndef BOX_H
#define BOX_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\entity\box.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\entity
 * Created Date: Monday, July 8th 2019, 7:07:30 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include <common.h>

void BOX_Draw(void);
void BOX_Create(void);
void BOX_Update(void);
#endif /* BOX_H */
