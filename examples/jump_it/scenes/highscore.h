/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\highscore.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, November 12th 2019, 2:27:14 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include <common.h>

extern scene scene_highscore;

int current_level_display;

typedef struct score
{
    char name[4];
    float score;
} score;

typedef struct level_scores
{
    int id;
    int _future;
    score scores[3];
} level_scores;

typedef struct score_header
{
    int version;
    int scores;
    char name[4];
} score_header;

void Highscore_Init(void);
void Highscore_AddScore(int level_id, float time);
level_scores *Highscore_GetLevel(int id);

#endif /* HIGHSCORE_H */
