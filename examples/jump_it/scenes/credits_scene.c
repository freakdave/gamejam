/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\Credits.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "credits_scene.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static void Credits_Init(void);
static void Credits_Destroy(void);
static void Credits_Update(float time);
static void Credits_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(scene_credits, &Credits_Init, &Credits_Destroy, &Credits_Render2D, NULL, &Credits_Update, SCENE_BLOCK);

float time_Credits;

extern sprite logo_dreamon;
extern sprite logo_neodc;

static void Credits_Init(void)
{
    time_Credits = Sys_FloatTime() + 1.0f;
}

static void Credits_Destroy(void)
{

}

static void Credits_Update(float time)
{
    (void)time;
    float now = Sys_FloatTime();
    if (now - time_Credits > 0.25f)
    {
        if (INPT_Button(0))
        {
            SCN_ChangeTo(main_title);
            time_Credits = now;
        }
    }
}

static void Credits_Render2D(float time)
{
    (void)time;
    UI_TextSize(32);
    UI_DrawStringCentered(640 - 320, 120, "Credits");
    UI_TextSize(24);

    static bool toggle_text = 0;
    if (Sys_Frames() % 12 == 0)
    {
        toggle_text = !toggle_text;
    }
    if (toggle_text)
    {
        UI_TextColor(0.0f, 1.0f, 0.0f);
    }
    else
    {
        UI_TextColor(0.0f, 0.0f, 1.0f);
    }
    UI_TextSize(20);
    UI_DrawStringCentered(640 - 320, 180, "Engine & Game");
    UI_DrawStringCentered(640 - 320, 220, "Hayden Kowalchuk / mrneo240");
    UI_TextColor(1.0f, 1.0f, 1.0f);
    UI_DrawStringCentered(640 - 320, 240, "Sources released Xmas `19");
    UI_DrawStringCentered(640 - 320, 300, "Huge thanks to:");
    UI_DrawStringCentered(640 - 320, 332, "EVERYONE in Simulant Discord");
    UI_DrawStringCentered(640 - 320, 364, "Sega, Sony and Microsoft");
    UI_DrawStringCentered(640 - 320, 396, "\x006"
                                          "DREAM ON 2019!"
                                          "\006");
    UI_TextSize(16);

    static int logo_x = 3;
    static int dir = 1;
    dimen_RECT pos = {logo_x, 480 - 36, 256, 36};
    UI_DrawTransSprite(&pos, 1.0f, &logo_dreamon);
    dimen_RECT pos2 = {640 - 256 - logo_x, 36, 256, 36};
    UI_DrawTransSprite(&pos2, 1.0f, &logo_neodc);
    logo_x += dir * 3;
    if (logo_x >= 640 - 256)
    {
        dir = -1;
    }
    if (logo_x <= 1)
    {
        dir = 1;
    }
}