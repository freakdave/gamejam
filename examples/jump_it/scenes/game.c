/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\game.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:49:41 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "game.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

#include "world_2d/world.h"
#include "world_2d/player.h"
#include "world_2d/Box2DIn3D.h"

static void Game_Init(void);
static void Game_Update(float time);
static void Game_Render2D(float time);
static void Game_Render3D(float time);

/* Registers the Title scene and populates the struct */
SCENE(scene_game, &Game_Init, NULL, &Game_Render2D, &Game_Render3D, &Game_Update, SCENE_BLOCK);

char dir;
float adjust[3] = {0, 22, 0};

extern sprite logo_dreamon;

//static int profileFrame = 0;
static int firstRun = 0;
extern void profiler_init(const char *output);
extern void profiler_start(void);
extern bool profiler_stop(void);

static void Game_Init()
{
    if (!firstRun)
        WRLD_2D_Create();
    firstRun++;

    win = 0;
    time_accum = 0.0f;
    time_clear = 0.0f;
    glm_vec3_copy((vec3){16.0f, 6.0f, 10.0f}, win_zoom);
    win_zoom_t = 2.0f;
    PLYR_2D_DisableJump();

    visible = 0;
    _radius = 16.0f;
    //profiler_init("/pc/gmon.out");
    //profiler_start();
}

static void Game_Update(float time)
{
    if (!win)
    {
        //2D
        WLRD_2D_Update(time);
        PLYR_2D_Update();
    }
    if (INPT_ButtonEx(BTN_X, BTN_RELEASE))
    {
        //adjust[1]++;
    }
    if (INPT_ButtonEx(BTN_Y, BTN_RELEASE))
    {
        //adjust[1]--;
        //profileFrame = 1;
        //CYG_PrintCallTrace();
        //CYG_Deinit();
        //profiler_stop();
    }

    /* Pause Checking */
    if (INPT_ButtonEx(BTN_START, BTN_PRESS) && !win)
    {
        push(&scene_root, scene_game);
        SCN_ChangeTo(scene_pause);
    }

    if (win && INPT_Button(BTN_B))
    {
        WRLD_2D_Destroy();
        SCN_ChangeTo(scene_highscore);
    }

/* Setup Logging */
#if 0
    if(profileFrame == 0){
        CYG_PrintCallTrace();
        CYG_Deinit();
        profileFrame--;
    }
    if (profileFrame > 0)
    {
        CYG_Init();

        profileFrame--;
    }
#endif
}

static void Game_Render2D(float time)
{
    (void)time;

    UI_TextColor(1.0f, 1.0f, 1.0f);
    char message[32];
    memset(message, 0, 32);

    /* Logos */
    dimen_RECT pos = {640 - 256, 480 - 36, 256, 36};
    UI_DrawTransSprite(&pos, 1.0f, &logo_dreamon);

    if (time_accum < READY_TIME)
    {

        UI_TextSize(32);
        snprintf(message, 32, "%2.03fs", (double)(READY_TIME - time_accum));
        UI_TextColor(1.0f, 1.0f, 1.0f);
        UI_DrawStringCentered(640 - 320, 240, "Get Ready!");
        if (time_accum >= READY_TIME / 2)
        {
            UI_TextColor(1.0f, 0.0f, 0.0f);
        }
        UI_DrawStringCentered(640 - 320, 240 + 80, message);
        UI_TextSize(16);
    }

    /* Winning Text */
    if (win)
    {
        //UI_DrawTransSprite((640 / 2) - (winner.i_width), (480 / 2) - (winner.i_height), winner.i_width * 2, winner.i_height * 2, 1.0f, &winner);
        snprintf(message, 32, "TIME: %2.03fs", (double)time_clear);
        UI_TextColor(1.0f, 1.0f, 1.0f);

        UI_TextSize(32);
        UI_DrawStringCentered(640 - 320, 80, message);
        UI_TextSize(24);
        UI_DrawStringCentered(640 - 320, 380, "Press " STRING_B_BTN " to Return!");
        UI_TextSize(16);
    }
}

static void Game_Render3D(float time)
{
    (void)time;

    if (time_accum < READY_TIME)
    {
        return;
    }

    glPushMatrix();

    vec3 position = {0, 0, 0};
    rigidbody_2d *player = PLYR_2D_Get();
    if (player)
        RBDY_2D_GetPosition_OGL(player, position);

    float f = player->_force[0];
    if (f > 500.0f)
    {
        f = 500;
    }
    else if (f < -500.0f)
    {
        f = -500;
    }

    adjust[0] = 8 * (f / 500);
    if (!win)
    {
#if 0
        if (position[1] > 14.0f)
        {
            gluLookAt(position[0] - adjust[0], position[1] + adjust[1] - _radius + 4, 16,
                      6.0f + position[0], position[1], 0.0f,
                      0.0f, 1.0f, 0.0f); // angled side from front
        }
        else
        {
            gluLookAt(position[0] - adjust[0], _radius + 4, 16, 6.0f + position[0], 10.0f, 0.0f, 0.0f, 1.0f, 0.0f); // angled side from front
        }
#endif
        gluLookAt(position[0] - adjust[0], position[1] + adjust[1] - _radius + 4, 16,
                  6.0f + position[0], position[1], 0.0f,
                  0.0f, 1.0f, 0.0f); // angled side from front
    }
    else
    {
        win_zoom_dt -= time;
        if (win_zoom_dt < 0.0f)
            win_zoom_dt = 0;

        vec3 cam_adjust;
        glm_vec3_copy((vec3){
                          (win_zoom[0] * (win_zoom_dt / win_zoom_t)) - win_zoom[0],
                          (win_zoom[1] * (win_zoom_dt / win_zoom_t)),
                          (win_zoom[2] * (win_zoom_dt / win_zoom_t)) - win_zoom[2],
                      },
                      cam_adjust);

        gluLookAt(8.0f + position[0] + cam_adjust[0], position[1] + 4 + cam_adjust[1], 16 + cam_adjust[2], 6.0f * (win_zoom_dt / win_zoom_t) + position[0], position[1], 0.0f, 0.0f, 1.0f, 0.0f); // Slowly Zooms in
    }

    BOX2D_DrawIn3D();
    PLYR_2D_Draw();
    glPopMatrix();
}
