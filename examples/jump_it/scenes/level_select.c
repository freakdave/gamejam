/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\level_select.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 17th 2019, 10:11:21 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "level_select.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

#include "world_2d/world.h"

static unsigned char selected_index;
static unsigned char last_select;
static float time_menu;
static float time_show;
bool _fade_in = false;

extern sprite square, square_trans;

static const char *headers[3] = {"Basic", "Levels", "Extras"};

static void LevelSel_Init(void);
static void LevelSel_Update(float time);
static void LevelSel_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(level_select, &LevelSel_Init, NULL, &LevelSel_Render2D, NULL, &LevelSel_Update, SCENE_BLOCK);

static void LevelSel_Init(void)
{
    selected_index = 0;
    last_select = 0;
    time_menu = 0;
    time_show = 0;
}

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)       \
    (byte & 0x80 ? '1' : '0'),     \
        (byte & 0x40 ? '1' : '0'), \
        (byte & 0x20 ? '1' : '0'), \
        (byte & 0x10 ? '1' : '0'), \
        (byte & 0x08 ? '1' : '0'), \
        (byte & 0x04 ? '1' : '0'), \
        (byte & 0x02 ? '1' : '0'), \
        (byte & 0x01 ? '1' : '0')

static void LevelSel_Update(float time)
{
    float now = Sys_FloatTime();

    if (now - time_menu > 0.12f)
    {
        extern void SND_Jump(void);

        dpad_t dpad = INPT_DPAD() & 15;

        /* Handle moving the selector indicator */
        switch (dpad)
        {
        case DPAD_LEFT:
            if (selected_index != 0)
                selected_index--;
            if (!WRLD_2D_LevelAvailable(selected_index))
            {
                selected_index++;
            }
            SND_Jump();
            break;
        case DPAD_RIGHT:
            selected_index++;
            if (!WRLD_2D_LevelAvailable(selected_index))
            {
                selected_index--;
            }
            SND_Jump();
            break;
        case DPAD_UP:
            selected_index -= 3;
            if (!WRLD_2D_LevelAvailable(selected_index))
            {
                selected_index += 3;
            }
            SND_Jump();
            break;
        case DPAD_DOWN:
            selected_index += 3;
            if (!WRLD_2D_LevelAvailable(selected_index))
            {
                selected_index -= 3;
            }
            SND_Jump();
            break;
        }

        if (dpad)
        {
            time_menu = now;
        }

        if (selected_index > 8)
        {
            selected_index = 8;
        }
    }

    /* Begin the selected level */
    if (INPT_ButtonEx(BTN_A, BTN_PRESS) && WRLD_2D_LevelAvailable(selected_index))
    {
        if (selected_index >= 6)
        {
            /* Alternate things */
            switch (selected_index)
            {
            case 6:
                SCN_ChangeTo(scene_input_test);
                break;
            case 7:
                SCN_ChangeTo(scene_spinner);
                break;
            case 8:
                break;
            }
        }
        else
        {
            /* Do actual levels */
            WRLD_2D_SetLevel(selected_index + 1);
            SCN_ChangeTo(scene_game);
        }
    }

    /* Go back to title screen */
    if (INPT_ButtonEx(BTN_B, BTN_PRESS))
    {
        SCN_ChangeTo(main_title);
    }

    time_show -= time;
}

static void LevelSel_Render2D(float time)
{
    (void)time;
    static float _next = 0;
    float now = Sys_FloatTime();

    if (now > _next)
    {
        _next = now + 0.5f;
        _fade_in = !_fade_in;
    }

    /* Background Teal */
    UI_DrawFill(&rect_pos_fullscreen, 173, 255, 255);

    UI_TextSize(24);
    UI_TextColor(1.0f, 1.0f, 1.0f);
    UI_DrawStringCentered(400 / 2, UI_TextSizeGet(), "Level Select");

    UI_TextSize(16);
    dimen_RECT pos = {400, 0, 2, 480};
    DrawQuad_NoTex(&pos);
    UI_DrawStringCentered(400 + (640 - 400) / 2, 16, "Info:");

    /* Level Grid */
    const int image_size = 64;
    const int spacing_w = 48;
    const int spacing_h = 48;

    const int start_x = 104;
    const int start_y = 116;

    UI_TextSize(32);
    UI_TextColor(1.0f, 1.0f, 0.0f);

    /* Draw the 3x3 Grid, then do headers */
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            /* Setup our color for each tile */
            UI_TextColorEx(1.0f, 1.0f, 1.0f, 1.0f);

            /* Draw Background Square*/
            dimen_RECT pos2 = {start_x + (i * (spacing_w + image_size)), start_y + (j * (spacing_h + image_size)), image_size, image_size};
            UI_DrawTransSprite(&pos2, 1.0f, &square);
            UI_TextColor(1.0f, 1.0f, 0.0f);
            float _alpha = (_fade_in ? (0.5f - (_next - now)) / 1.0f : ((_next - now)) / 1.0f);
            if (selected_index == (i + (j * 3)))
            {
                UI_TextColorEx(1.0f, 0, 0, _alpha + 0.5f);
            }
            if (!WRLD_2D_LevelAvailable(i + (j * 3)))
            {
                UI_TextColor(0.564f, 0.564f, 0.564f);
            }

            UI_DrawCharacterCentered(start_x + (i * (spacing_w + image_size)) + image_size / 2, start_y + (j * (spacing_h + image_size)) + (image_size / 2), '1' + (i + (3 * j)));
        }
        UI_TextColorEx(1.0f, 1.0f, 0.0f, 1.0f);
        UI_DrawString(4, start_y - 36 + (i * (spacing_h + image_size)), headers[i]);
    }

    /* Draw the Record Data */
    UI_TextSize(16);
    level_scores *data = Highscore_GetLevel(level_ids[selected_index]);
    if (data)
    {
        char line[16];
        for (int i = 0; i < 3; i++)
        {
            snprintf(line, 16, "%s    %6.03f", data->scores[i].name, (double)data->scores[i].score);
            UI_DrawStringCentered(400 + (640 - 400) / 2, 60 + (20 * i), line);
        }
    }
    else
    {
        UI_DrawStringCentered(400 + (640 - 400) / 2, 60 + (20 * 0), "No Data!");
    }
}
