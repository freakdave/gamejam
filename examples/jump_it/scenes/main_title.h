#ifndef MAIN_TITLE_H
#define MAIN_TITLE_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\main_title.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, December 31st 2019, 6:44:57 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include <common.h>

extern scene main_title;
#endif /* MAIN_TITLE_H */
