/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\jump_it\scenes\scenes.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\jump_it\scenes
 * Created Date: Wednesday, January 22nd 2020, 7:24:32 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef SCENES_H
#define SCENES_H

#include "credits_scene.h"
#include "game.h"
#include "highscore.h"
#include "intro_scene.h"
#include "input_test.h"
#include "level_select.h"
#include "main_title.h"
#include "pause.h"
#include "save_scene.h"
#include "vmu_dc.h"
#include "spinner_scene.h"

#endif /* SCENES_H */
