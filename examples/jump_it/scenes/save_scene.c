/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "save_scene.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static unsigned char selected_index;
static unsigned char step;
static float time_menu;
static const char *headers[4] = {"Setup", "Slow", "Fast", "Quit"};
static const char *steps[3] = {"VMU Setup!", "Reading Slow", "Reading Fast"};

static void Save_Init(void);
static void Save_Update(float time);
static void Save_Render2D(float time);

//static int our_files_sector[100];
static int our_files_num;
static char empty_buffer[512];
static char msg[32];

#ifdef _arch_dreamcast
#include <dc/maple.h>
#include <dc/maple/vmu.h>
#include <dc/vmufs.h>

static vmu_root_t root_buf;
static maple_device_t *vmu_dev = NULL;
static vmu_dir_t *dir_buf;

/* Here's the actual meat of it */
void write_entry()
{
#define VMU_SIZE 4096
    vmu_pkg_t pkg;
    uint8 data[5120], *pkg_out;
    int pkg_size;
    int i;
    file_t f;

    strcpy(pkg.desc_short, "VMU Test");
    strcpy(pkg.desc_long, "This is a test VMU file");
    strcpy(pkg.app_id, "gamejam");
    pkg.icon_cnt = 0;
    pkg.icon_anim_speed = 0;
    pkg.eyecatch_type = VMUPKG_EC_NONE;
    pkg.data_len = VMU_SIZE;
    pkg.data = data;

    for (i = 0; i < VMU_SIZE; i++)
        data[i] = i & 255;

    vmu_pkg_build(&pkg, &pkg_out, &pkg_size);

    char filename[17] = "/vmu/d2/TESTFILE";

    for (i = 0; i < 10; i++)
    {
        filename[15] = '0' + i;
        filename[16] = '\0';
        fs_unlink(filename);
        f = fs_open(filename, O_WRONLY);

        if (!f)
        {
            printf("error writing\n");
            return;
        }

        fs_write(f, pkg_out, pkg_size);
        printf("Writing Save! %s\n", &filename[8]);
        fs_close(f);
    }
}

static void read_vmu_fast(void)
{
    if (vmu_block_read(vmu_dev, 255, (uint8_t *)&root_buf) == MAPLE_EOK)
    {
        int need_bytes = vmufs_dir_blocks(&root_buf);
        int file_entries = need_bytes / 32; // >> 5
        //printf("Read Root and needed %d bytes for %d saves\n", need_bytes, file_entries);
        dir_buf = (vmu_dir_t *)malloc(need_bytes);
        vmufs_dir_read(vmu_dev, &root_buf, dir_buf);
        //int ret = vmufs_dir_read(vmu_dev, &root_buf, dir_buf);
        //printf("Read Dirs with result %d, 1st file %s\n", ret, dir_buf[0].filename);
        char temp[7];
        memset(temp, 0, sizeof(temp));
        for (int i = 0; i < file_entries; i++)
        {
            if (dir_buf[i].filesize != 0)
            {
                strlcpy(temp, dir_buf[i].filename, 7);
                //printf("found file: %s from %s\n", temp, dir_buf[i].filename);
                if (!strcmp(temp, "TESTFI"))
                {
                    vmu_block_read(vmu_dev, dir_buf[i].firstblk + 1, (uint8_t *)&empty_buffer);
                    printf("Found Save! %s\n", dir_buf[i].filename);
                }
            }
        }
    }
    else
    {
        printf("ERROR READING ROOT!");
    }
}
#else

static int *vmu_dev = NULL;

static void read_vmu_fast(void)
{
}
#endif

#ifdef _arch_dreamcast
static void setup_vmu(void)
{
    printf("Writing Saves to VMU D2!\n");
    write_entry();
    printf("....DONE!\n");
    /*
    for (int i = 0; i < 100; i++)
    {
        our_files_sector[i] = -1;
    }*/
}
#else
static void setup_vmu(void)
{
}
#endif

#ifdef _arch_dreamcast
static void read_vmu_slow(void)
{
    FILE *fp;

    char filename[17] = "/vmu/d2/TESTFILE";
    for (int i = 0; i < 10; i++)
    {
        filename[15] = '0' + i;
        filename[16] = '\0';
        if (!(fp = fopen(filename, "rb")))
        {
            printf("Found Save! %s\n", filename);
            fclose(fp);
        }
    }
}
#else
static void read_vmu_slow(void)
{
}
static void vmu_beep_raw(void* ptr, unsigned int a){
    (void)ptr;
    (void)a;
    (void)empty_buffer;
}
#endif

void Save_Register(void)
{
    save_scene = (scene){
        .init = &Save_Init,
        .render2D = &Save_Render2D,
        .render3D = NULL,
        .update = &Save_Update,
        .flags = 0,
    };
}

static void Save_Init(void)
{
    selected_index = 0;
    time_menu = 0;
    step = 0;
    our_files_num = 0;
    memset(msg, 0, sizeof(msg));
#ifdef _arch_dreamcast
    vmu_dev = maple_enum_dev('d' - 'a', '2' - '0');
#endif
}

static void Save_Update(float time)
{
    float now = Sys_FloatTime();
    (void)time;

    /* Handle input every so often */
    if (now - time_menu > 0.12f)
    {
        dpad_t dpad = INPT_DPAD() & 15;
        switch (dpad)
        {
        case DPAD_UP:
            if (selected_index != 0)
                selected_index--;
            time_menu = now;
            break;
        case DPAD_DOWN:
            if (selected_index != 3)
                selected_index++;

            time_menu = now;
            break;
        }
    }
    /* Execute Action */
    if (INPT_ButtonEx(BTN_A, BTN_PRESS))
    {
        float a;
        switch (selected_index)
        {
        case 0:
            step = 0;
            memset(msg, 0, sizeof(msg));
            vmu_beep_raw(vmu_dev, 0x000065f0);
            a = Sys_FloatTime();
            setup_vmu();
            a = Sys_FloatTime() - a;
            vmu_beep_raw(vmu_dev, 0x000065f0);
            snprintf(msg, 32, "Took %2.03fs", (double)a);
            printf("%s",msg);
            break;
        case 1:
            if (vmu_dev != NULL)
            {
                step = 1;
                memset(msg, 0, sizeof(msg));
                vmu_beep_raw(vmu_dev, 0x000065f0);
                a = Sys_FloatTime();
                read_vmu_slow();
                a = Sys_FloatTime() - a;
                vmu_beep_raw(vmu_dev, 0x000065f0);
                snprintf(msg, 32, "Took %2.03fs", (double)a);
                printf("%s",msg);
            }
            break;
        case 2:
            if (vmu_dev != NULL)
            {
                step = 2;
                memset(msg, 0, sizeof(msg));
                vmu_beep_raw(vmu_dev, 0x000065f0);
                a = Sys_FloatTime();
                read_vmu_fast();
                a = Sys_FloatTime() - a;
                vmu_beep_raw(vmu_dev, 0x000065f0);
                snprintf(msg, 32, "Took %2.03fs", (double)a);
                printf("%s",msg);
                
            }
            break;
        case 3:
            Sys_Quit();
            break;
        }
    }
}

static void Save_Render2D(float time)
{
    (void)time;

    /* Background Teal */
    UI_DrawFill(&rect_pos_fullscreen, 173, 255, 255);

    /* Menu Options */
    const int image_size = 32;
    const int spacing_h = 24;

    const int start_x = 4;
    const int start_y = 4;

    UI_TextSize(16);
    if (strlen(msg) != 0)
    {
        UI_DrawString(2, 300, msg);
    }
    UI_DrawString(2, 440, steps[step]);
    UI_TextColorEx(1, 1, 1, 0.5f);
    UI_DrawString(2, 460, "Hayden Kowalchuk / mrneo240 '19");

    UI_TextSize(32);

    for (int i = 0; i < 4; i++)
    {
        UI_TextColor(1.0f, 1.0f, 0.0f);
        if (selected_index == i)
        {
            UI_TextColorEx(0.5f, 0.5f, 1.0f, 1.0f);
        }

        UI_DrawString(start_x, start_y + (i * (spacing_h + image_size)), headers[i]);
    }
    // http://freestreams-live1.com/ufc/
    if (vmu_dev == NULL)
    {
        /* Draw Box */
        int height = 200;
        int width = (int)(height * 1.5f);

        dimen_RECT quad = {.x = 640 / 2 - width / 2, .y = 480 / 2 - height / 2, .w = width, .h = height};
        UI_DrawFill(&quad, 0.0f, 0.0f, 0.0f);

        /* Now Text */
        UI_TextColor(1.0f, 0, 0);
        UI_TextSize(32);
        UI_DrawStringCentered(640 / 2, 480 / 2 - UI_TextSizeGet(), "ERROR");
        UI_TextSize(20);
        UI_DrawStringCentered(320 + 32, 270, " Things are broken");
    }
}
