/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\Box2DIn3D.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Thursday, November 14th 2019, 2:53:43 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "Box2DIn3D.h"
#include "renderer.h"
#include "input.h"

#include "2d_physics/2d_physics.h"
#include "common/cube_data.h"

extern GLfloat _box_vertices3[144];
extern GLubyte _box_indices[36];

extern int boxes_2d;
extern rigidbody_2d **bodies_2d;

void BOX2D_DrawIn3D()
{
    if (boxes_2d <= 0)
    {
        return;
    }

    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    //glDisableClientState(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3 + 3);
    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3);

    for (int i = 0; i < boxes_2d - 2; i++)
    {
        glPushMatrix();
        //float x = 2*(bodies_2d[i]->shape.max.x - bodies_2d[i]->shape.min.x) / 2 + bodies_2d[i]->shape.min.x;
        //float y = 480 - ((bodies_2d[i]->shape.max.y - bodies_2d[i]->shape.min.y) / 2 + bodies_2d[i]->shape.min.y);

        float width = RBDY_2D_GetWidth(bodies_2d[i]);
        float height = RBDY_2D_GetHeight(bodies_2d[i]);

        vec3 position;
        RBDY_2D_GetPosition_OGL(bodies_2d[i], position);
        if (width > 25)
        {
            //x=0;
            //printf("pos: {%f, %f, %f}\n", position[0], position[1], position[2]);
        }
        //glTranslatef(x / 32, y / 24, 0);
        //glScalef(width / 25, height / 25, 1);
        glTranslatef(position[0], position[1], position[2]);

        //
        //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
        //glTranslatef(-width * (32.0f/25.0f) / 2.0f, 0, 0);
        glScalef(width / 25.0f, height / 25.0f, 1);
        //printf("position: %f, adjustment: %f\n", position[0], position[0] - (width * (32.0f/25.0f) / 2.0f));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);

        glPopMatrix();
    }

    glPushMatrix();
    float width = RBDY_2D_GetWidth(bodies_2d[boxes_2d - 2]);
    float height = RBDY_2D_GetHeight(bodies_2d[boxes_2d - 2]);
    vec3 position;
    RBDY_2D_GetPosition_OGL(bodies_2d[boxes_2d - 2], position);

    glTranslatef(position[0], position[1] - 3, position[2] + 0.1f);
    glScalef(width / 25.0f, height / 25.0f, 1);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);

    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glPopMatrix();
}
