/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\world.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Monday, October 14th 2019, 6:12:08 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "world.h"

#include <cglm/cglm.h>
#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <common/2d_physics/2d_physics.h>
#include <ui/ui_backend.h>

#include "player.h"
#include "scenes/highscore.h"

static rigidbody_2d *bodies[100];
static int boxes = 0;
static int current_level;
int current_level_id;

int boxes_2d;
rigidbody_2d **bodies_2d;

vec3 GRAVITY;
float time_accum;
float time_clear;

static int init = 0;

void WRLD_2D_Create(void)
{
    time_accum = 0;
    time_clear = 0;
    current_level = 1;
    PLYR_2D_DisableJump();
    PHYS_2D_Create();
}

void WRLD_2D_Destroy(void)
{
    memset(bodies, 0, sizeof(bodies));
    bodies_2d = NULL;
    boxes = 0;
    init = 0;
    boxes_2d = 0;
    time_accum = 0;
    time_clear = 0;
    PHYS_2D_Destroy();
    PLYR_2D_Destroy();
    PLYR_2D_DisableJump();
}

void WRLD_2D_Draw(void)
{
    //rigidbody_2d *local_bodies = PHYS_2D_get_bodies();

    glEnable(GL_BLEND);
    for (int i = 0; i < PHYS_2D_get_active(); i++)
    {
        //UI_DrawTexturedQuad(local_bodies[i].shape.min.x, local_bodies[i].shape.min.y, local_bodies[i].shape.max.x - local_bodies[i].shape.min.x, (local_bodies[i].shape.max.y) - (local_bodies[i].shape.min.y), &_square_inv);
    }
    glDisable(GL_BLEND);
}

void (*WRLD_CustomUpdate)(void) = NULL;

int WORLD_2D_GetLevelID(void)
{
    return current_level_id;
}

int WORLD_2D_GetLevel(void)
{
    return current_level;
}

void WRLD_2D_SetLevel(int id)
{
    current_level = id;
}

void win_collide(void *a, void *b)
{
    rigidbody_2d *_a = (rigidbody_2d *)a;
    (void)b;
    _a->layer = 0;
    extern int win;
    extern float win_zoom_dt;
    if (!win)
    {
        win = 1;

        time_clear = time_accum - READY_TIME;
        win_zoom_dt = 2.0f;
        Highscore_AddScore(WORLD_2D_GetLevelID(), time_clear);
        current_level_display = WORLD_2D_GetLevelID();
        PLYR_2D_DisableJump();
    }
}

#include "levels.h"

void WLRD_2D_Update(float time)
{
    PHYS_2D_Tick(time);
    time_accum += time;

    if (!init && time_accum > READY_TIME)
    {
        /* Remove a possible custom update */
        WRLD_CustomUpdate = NULL;

        /* Grab our level creation function and call */
        void (*create_ptr)(void) = level_create[current_level - 1];
        (*create_ptr)();

        init = 1;
        boxes_2d = boxes;

        bodies_2d = bodies;
        PLYR_2D_Create();
    }
    if (WRLD_CustomUpdate)
        (*WRLD_CustomUpdate)();
}
