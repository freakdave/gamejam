/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter\slide_message1.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter
 * Created Date: Tuesday, December 3rd 2019, 4:36:36 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#ifndef SLIDE_MESSAGE1_H
#define SLIDE_MESSAGE1_H

#include <common.h>

extern scene sl_Message1;

#endif /* SLIDE_MESSAGE1_H */
