
#include "slide_message1.h"

#include "base.h"

static void SL_Message1_Init(void);
static void SL_Message1_Exit(void);
static void SL_Message1_Update(float time);
static void SL_Message1_Render2D(float time);

SCENE(sl_Message1, &SL_Message1_Init, &SL_Message1_Exit, &SL_Message1_Render2D, NULL, &SL_Message1_Update, SCENE_BLOCK);

static tx_image *sl_internal;
static int slide_step;
static float time_fade_in;

static void SL_Message1_Init(void)
{
    printf("SLIDE 1 ENTER\n");
    sl_internal = IMG_load("sl_internal.png");
    RNDR_CreateTextureFromImage(sl_internal);
    slide_step = 0;
    time_fade_in = 0;
}

static void SL_Message1_Exit(void)
{
    printf("SLIDE 1 EXIT\n");
    resource_object_remove(sl_internal->crc);
}

static void SL_Message1_Update(float time)
{
    if (time_fade_in >= time)
    {
        time_fade_in -= time;
    }

    if (slide_step == 6)
    {
        SCN_ChangeTo(sl_Message2);
    }
    if (INPT_ButtonEx(BTN_A, BTN_PRESS))
    {
        time_fade_in = 2.0f;
        slide_step++;
    }
}

static void SL_Message1_Render2D(float time)
{
    (void)time;
    UI_TextColorEx(1, 1, 1, 1);
    UI_TextSize(18);
    UI_DrawPicDimensions(&rect_pos_fullscreen, sl_internal);
    switch (slide_step)
    {
    case 5:
        UI_TextSize(32);
        UI_DrawStringCentered(640 - 32, 480 - 32, "\x00D");
        UI_TextSize(18);
        FALLTHROUGH
    case 4:
        if (slide_step == 4)
            UI_TextAlpha((2.0f - time_fade_in) / 2.0f);
        UI_DrawString(24, 80 + (4 * 64), "\x008 Lower the barrier to entry");
        UI_TextAlpha(1.0f);
        FALLTHROUGH
    case 3:
        if (slide_step == 3)
            UI_TextAlpha((2.0f - time_fade_in) / 2.0f);
        UI_DrawString(24, 80 + (3 * 64), "\x008 Open Source the engine+examples");
        UI_TextAlpha(1.0f);
        FALLTHROUGH
    case 2:
        if (slide_step == 2)
            UI_TextAlpha((2.0f - time_fade_in) / 2.0f);
        UI_DrawString(24, 80 + (2 * 64), "\x008 Execute on DC & PSP");
        UI_TextAlpha(1.0f);
        FALLTHROUGH
    case 1:
        if (slide_step == 1)
            UI_TextAlpha((2.0f - time_fade_in) / 2.0f);
        UI_DrawString(24, 80 + (1 * 64), "\x008 Write a game engine");
        UI_TextAlpha(1.0f);
        FALLTHROUGH
    case 0:
        UI_TextSize(32);
        UI_DrawStringCentered(320, 32, "Goals");
        break;
    }
}