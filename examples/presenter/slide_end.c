#include "slide_end.h"

#include "base.h"

static void SL_End_Init(void);
static void SL_End_Update(float time);
static void SL_End_Render2D(float time);

SCENE(sl_end, &SL_End_Init, NULL, &SL_End_Render2D, NULL, &SL_End_Update, SCENE_BLOCK);

static int slide_step;

static void SL_End_Init(void)
{
    slide_step = 0;
}

static void SL_End_Update(float time)
{
    (void)time;
    //if (INPT_Button(0))
    if (INPT_ButtonEx(BTN_A, BTN_RELEASE) || INPT_ButtonEx(BTN_START, BTN_RELEASE))
    {
        Sys_Quit();
    }
}

static void SL_End_Render2D(float time)
{
    (void)time;
    UI_TextSize(14);
    UI_DrawStringCentered(320, 24, "End of slide show, click to Exit");
}