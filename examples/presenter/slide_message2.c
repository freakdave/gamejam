#include "slide_message2.h"
#include "base.h"
#include "../../common/input.h"
#include "../../ui/ui_backend.h"

static void SL_Message2_Init(void);
static void SL_Message2_Exit(void);
static void SL_Message2_Update(float time);
static void SL_Message2_Render2D(float time);

SCENE(sl_Message2, &SL_Message2_Init, &SL_Message2_Exit, &SL_Message2_Render2D, NULL, &SL_Message2_Update, SCENE_BLOCK);

static tx_image *sl_internal;
static int slide_step;
static float time_slide_in;

static void SL_Message2_Init(void)
{
    sl_internal = IMG_load("sl_internal.png");
    RNDR_CreateTextureFromImage(sl_internal);
    slide_step = 0;
    time_slide_in = 0;
}

static void SL_Message2_Exit(void)
{
    resource_object_remove(sl_internal->crc);
}

static void SL_Message2_Update(float time)
{
    if (time_slide_in >= 0)
    {
        time_slide_in -= time;
    }

    if (slide_step == 6)
    {
        SCN_ChangeTo(sl_end);
    }
    if (INPT_ButtonEx(BTN_A, BTN_PRESS))
    {
        time_slide_in = 1.5f;
        slide_step++;
    }
}

static void SL_Message2_Render2D(float time)
{
    (void)time;
    UI_TextColorEx(1, 1, 1, 1);
    UI_TextSize(16);
    UI_DrawPicDimensions(&rect_pos_fullscreen, sl_internal);
    float slide_amount = 0.0f;
    switch (slide_step)
    {
    case 5:
        UI_TextSize(32);
        UI_DrawStringCentered(640 - 32, 480 - 32, "\x00D");
        UI_TextSize(16);
        FALLTHROUGH
    case 4:;
        slide_amount = 0.0f;
        if (slide_step == 4)
            slide_amount = (80 + (4 * 64)) * time_slide_in / 1.5f;
        UI_DrawString(24, 80 + (4 * 64) - slide_amount, "\x006 Write all new code");
        FALLTHROUGH
    case 3:;
        slide_amount = 0.0f;
        if (slide_step == 3)
            slide_amount = (80 + (4 * 64)) * time_slide_in / 1.5f;
        UI_DrawString(24, 80 + (3 * 64) - slide_amount, "\x006 Let the user be in control");
        FALLTHROUGH
    case 2:;
        slide_amount = 0.0f;
        if (slide_step == 2)
            slide_amount = (80 + (4 * 64)) * time_slide_in / 1.5f;
        UI_DrawString(24, 80 + (2 * 64) - slide_amount, "\x006 Keep things small but powerful");
        FALLTHROUGH
    case 1:;
        slide_amount = 0.0f;
        if (slide_step == 1)
            slide_amount = (80 + (4 * 64)) * time_slide_in / 1.5f;
        UI_DrawString(24, 80 + (1 * 64) - slide_amount, "\x006 Use Plain C99");
        FALLTHROUGH
    case 0:
        UI_TextSize(32);
        UI_DrawStringCentered(320, 32, "But How?");
        break;
    }
}