/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter\slide_title.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter
 * Created Date: Tuesday, December 3rd 2019, 4:06:39 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef SLIDE_TITLE_H
#define SLIDE_TITLE_H

#include <common.h>

extern scene slide_title;

#endif /* SLIDE_TITLE_H */
