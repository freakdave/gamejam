/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter\slide_message2.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter
 * Created Date: Wednesday, December 4th 2019, 8:21:03 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef SLIDE_MESSAGE2_H
#define SLIDE_MESSAGE2_H

#include <common.h>

extern scene sl_Message2;

#endif /* SLIDE_MESSAGE2_H */
