/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\json_world\json_scene.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\json_world
 * Created Date: Saturday, February 8th 2020, 7:14:14 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#include "json_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

#include "common/cube_data.h"
#include "jsmn.h"

extern GLfloat _box_vertices3[144];
extern GLubyte _box_indices[36];

WINDOW_TITLE("json_world", WINDOWED);

static void JSONWorld_Init(void);
static void JSONWorld_Exit(void);
static void JSONWorld_Update(float time);
static void JSONWorld_Render2D(float time);
static void JSONWorld_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&JSONWorld_Init, &JSONWorld_Exit, &JSONWorld_Render2D, &JSONWorld_Render3D, &JSONWorld_Update, SCENE_BLOCK);
SCENE(scene_spinner, &JSONWorld_Init, &JSONWorld_Exit, &JSONWorld_Render2D, &JSONWorld_Render3D, &JSONWorld_Update, SCENE_BLOCK);

static void Draw_Objects(void);
static void draw_cube(void);
static void draw_shadow(void);

typedef struct json_object {
  char name[24];
  char id[24];
  vec3f pos;
  vec3f rot;
  vec3f scale;
  float orig_scale;
  int type;
  model_obj *tree_obj;
  tx_image *texture;
} json_object;

json_object objs[32];
json_object camera;

tx_image *shadow_tex;
json_object cube;

vec3f camera_rot_base;
static int objects = 0;

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
  if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
      strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
    return 0;
  }
  return -1;
}

static __attribute__((unused)) char *jsmn_type(int type) {
  switch (type) {
    case 1:
      return "JSMN_OBJECT";
      break;
    case 2:
      return "JSMN_ARRAY";
      break;
    case 3:
      return "JSMN_STRING";
      break;
    case 4:
      return "JSMN_PRIMITIVE";
      break;
  }
  return "";
}

int JSON_ParseDropper(const char *filename) {
  objects = 0;
#ifdef __WIN32__
  FILE *input = fopen(transform_path(filename), "rb");
#else
  FILE *input = fopen(transform_path(filename), "rb");
#endif
  if (!input) {
    printf("Could not find file: %s\n", transform_path(filename));
    return 1;
  }

  fseek(input, 0L, SEEK_END);
  int flen = ftell(input);
  rewind(input);

  /* Create buffer */
  char *json_string = (char *)malloc(flen);
  memset(json_string, '\0', flen);

  /* Read our file */
  fread(json_string, flen, 1, input);
  fclose(input);

  int ret_tokens;
  jsmn_parser p;
  jsmntok_t tokens[512]; /* We expect no more than 512 tokens */

  jsmn_init(&p);
  ret_tokens = jsmn_parse(&p, json_string, flen, tokens, sizeof(tokens) / sizeof(tokens[0]));
  if (ret_tokens < 0) {
    printf("Failed to parse JSON: %d\n", ret_tokens);
    return 1;
  }

  /* Assume the top-level element is an object */
  if (ret_tokens < 1 || tokens[0].type != JSMN_OBJECT) {
    printf("Object expected\n");
    return 1;
  }

  int root_index;
  /* Loop over all keys of the root object */
  for (root_index = 1; root_index < ret_tokens; root_index++) {
    if (jsoneq(json_string, &tokens[root_index], "entities") == 0) {
      /* entities is the main array, holding all entity objects */
      if (tokens[root_index + 1].type != JSMN_ARRAY) {
        continue; /* We expect entities to be an array of objects */
      }

      printf("Found %d entities\n", tokens[root_index + 1].size);

      /* Loop through all elements in the entity array */
      int entity_start = 0;

      for (int entity_num = 0; entity_num < tokens[root_index + 1].size; entity_num++) {
        int offset_to_next_entity = 0;
        jsmntok_t *entity = &tokens[root_index + entity_start + 2];
        //printf("at %d found type %s with size %d\n", root_index + entity_start + 2, jsmn_type(entity->type), entity->size);
        if (entity->type != JSMN_OBJECT) {
          printf("ERROR key: %.*s\n", entity->end - entity->start, json_string + entity->start);
          continue; /* We expect the entity to be an object */
        }

        for (int entity_elements = 0; entity_elements < entity->size; entity_elements++) {
          /* This is the whole object */
          //jsmntok_t *entity = &tokens[root_index + entity_start + 2 + offset_to_next_entity];
          //printf("key: %.*s\n", entity->end - entity->start, json_string + entity->start);

          /* This is the first token of the object */
          jsmntok_t *attrs = &tokens[root_index + entity_start + 3 + offset_to_next_entity];

          if (jsoneq(json_string, attrs, "name") == 0) {
            /* We may use strndup() to fetch string value */
            /*   %.*s = len, data */
            //printf("- Name: %.*s\n", (attrs + 1)->end - (attrs + 1)->start, json_string + (attrs + 1)->start);
            strncpy(objs[objects].name, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);

            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "position") == 0) {
            jsmntok_t *position = attrs + 1;
            //printf("found position %s with size %d\n", jsmn_type(position->type), position->size);
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              //static const int max_length = 8;
              //int float_len = ((position + pos_elem + 1)->end - (position + pos_elem + 1)->start) > max_length ? max_length : ((position + pos_elem + 1)->end - (position + pos_elem + 1)->start);
              //char float_str[9];
              //strncpy(float_str, json_string + (position + pos_elem + 1)->start, float_len);

              //printf("- %.*s: %f == %.*s\n", (position + pos_elem)->end - (position + pos_elem)->start, json_string + (position + pos_elem)->start, dc_safe_atof(float_str), float_len, float_str);
              //*(float *)(&objs[objects].pos.x + pos_elem) = dc_safe_atof(float_str);
              //printf("- %.*s: %f == %.*s\n", (position + pos_elem)->end - (position + pos_elem)->start, json_string + (position + pos_elem)->start, dc_safe_atof(json_string + (position + pos_elem + 1)->start), float_len , json_string + (position + pos_elem + 1)->start);
              *(float *)(&objs[objects].pos.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            float temp = objs[objects].pos.y;
            objs[objects].pos.y = objs[objects].pos.z;
            objs[objects].pos.z = -temp;
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }
          if (jsoneq(json_string, attrs, "rotation") == 0) {
            jsmntok_t *position = attrs + 1;
            //printf("found rotation %s with size %d\n", jsmn_type(position->type), position->size);
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              //printf("R- %.*s: %f ==|%.*s|\n", (position + pos_elem)->end - (position + pos_elem)->start, json_string + (position + pos_elem)->start, dc_safe_atof(json_string + (position + pos_elem + 1)->start), (position + pos_elem + 1)->end - (position + pos_elem + 1)->start, json_string + (position + pos_elem + 1)->start);
              *(float *)(&objs[objects].rot.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            /* Translation */
            // x: pitch y: roll z: yaw
            //yaw   (rotation around)
            //pitch (looking up/down)
            //roll (rolling, not used for camera)
            if (!stricmp(objs[objects].name, "camera")) {
              objs[objects].rot.x *= -1;
              objs[objects].rot.x += M_PI / 2;
              objs[objects].rot.z *= -1;
              objs[objects].rot.z += M_PI / 2;
            }

            if (objs[objects].rot.x < 0.0f) {
              objs[objects].rot.x += 2 * M_PI;
            }
            if (objs[objects].rot.y < 0.0f) {
              objs[objects].rot.y += 2 * M_PI;
            }
            if (objs[objects].rot.z < 0.0f) {
              objs[objects].rot.z += 2 * M_PI;
            }
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }
          if (jsoneq(json_string, attrs, "scale") == 0) {
            jsmntok_t *position = attrs + 1;
            //printf("found scale %s with size %d\n", jsmn_type(position->type), position->size);
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              //printf("S- %.*s: %f ==|%.*s|\n", (position + pos_elem)->end - (position + pos_elem)->start, json_string + (position + pos_elem)->start, dc_safe_atof(json_string + (position + pos_elem + 1)->start), (position + pos_elem + 1)->end - (position + pos_elem + 1)->start, json_string + (position + pos_elem + 1)->start);
              *(float *)(&objs[objects].scale.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            //float temp = objs[objects].scale.y;
            //objs[objects].scale.y = objs[objects].scale.z;
            //objs[objects].scale.z = temp;
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }

          if (jsoneq(json_string, attrs, "_mesh") == 0) {
            char temp[64] = {0};
            strcat(temp, "assets/");
            strncat(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].tree_obj = OBJ_load(temp);
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          if (jsoneq(json_string, attrs, "_texture") == 0) {
            char temp[64] = {0};
            strcat(temp, "assets/");
            strncat(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].texture = IMG_load(temp);
            RNDR_CreateTextureFromImage(objs[objects].texture);
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          if (jsoneq(json_string, attrs, "_original_scale") == 0) {
            char temp[32] = {0};
            strncat(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].orig_scale = dc_safe_atof(temp);
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          /* garbage we dont care about */
          if (jsoneq(json_string, attrs, "friction") == 0) {
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "id") == 0) {
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "hardness") == 0) {
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "mass") == 0) {
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "source") == 0) {
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          printf("UNKNOWN key: %.*s\n", attrs->end - attrs->start, json_string + attrs->start);
          offset_to_next_entity += 2; /* Key+Value */
        }
        objects++;
        entity_start += offset_to_next_entity + 1;
      }
    } else {
      //printf("Unexpected key: %.*s\n", tokens[root_index].end - tokens[root_index].start, json_string + tokens[root_index].start);
      break;
    }
  }
  printf("\nFinished parsing!\n\nObjects parsed:\n");
  for (int l = 0; l < objects; l++) {
    if (objs[l].texture != NULL && objs[l].tree_obj != NULL) {
      //objs[l].tree_obj->texture = objs[l].texture->id;
    }
    printf("name: %s\t", objs[l].name);
    printf("pos[%f, %f, %f]\t", objs[l].pos.x, objs[l].pos.y, objs[l].pos.z);
    printf("rot[%f, %f, %f]\t", RAD2DEG(objs[l].rot.x), RAD2DEG(objs[l].rot.y), RAD2DEG(objs[l].rot.z));
    printf("scale[%f, %f, %f]\n", objs[l].scale.x, objs[l].scale.y, objs[l].scale.z);
  }
  camera = objs[0];
  for (int l = 0; l < objects; l++) {
    if (!stricmp(objs[l].name, "camera")) {
      camera = objs[l];
      camera_rot_base = camera.rot;
      break;
    }
  }
  return 0;
}

static void JSONWorld_Init(void) {
  memset(objs, '\0', sizeof(objs));
  if (!JSON_ParseDropper("assets/world.txt")) {
  } else {
    printf("ERROR!\n");
  }
  shadow_tex = IMG_load("assets/shadow_square.png");
  RNDR_CreateTextureFromImage(shadow_tex);

  cube.pos = (vec3f){0, 2, 0};
  cube.rot = (vec3f){0, 0, 0};
  cube.scale = (vec3f){5, 5, 5};
}

static void JSONWorld_Exit(void) {
}

vec3f camera_front;
vec3f camera_look;

static void JSONWorld_Update(float time) {
  (void)time;
  json_object *ref = &cube;  //&objs[2];
  //json_object *ref = &camera;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    //Sys_Quit();
    //printf("front[%f, %f, %f]\n", camera_look.x, camera_look.y, camera_look.z);
    printf("rot[%f, %f, %f]\t", ref->rot.x, ref->rot.y, ref->rot.z);
  }
#if 0

  /* basic camera controls */
  float _x = INPT_AnalogF(AXES_X);
  float _y = INPT_AnalogF(AXES_Y);
  float x = _x * SQRT(1 - _y * _y * 0.5f);
  float y = _y * SQRT(1 - _x * _x * 0.5f);

  ref->rot.z = camera_rot_base.z + ((M_PI / 2) * 0.8f) * x;
  ref->rot.x = camera_rot_base.x + ((M_PI / 2) * 0.8f) * y;

#else
  /* rotation */
  /*
  if (INPT_DPADDirection(DPAD_LEFT)) {
    ref->rot.z -= 0.02f;
  }
  if (INPT_DPADDirection(DPAD_RIGHT)) {
    ref->rot.z += 0.02f;
  }
  if (INPT_DPADDirection(DPAD_UP)) {
    ref->rot.x -= 0.02f;
  }
  if (INPT_DPADDirection(DPAD_DOWN)) {
    ref->rot.x += 0.02f;
  }
  */
  /* position */
  if (INPT_DPADDirection(DPAD_LEFT)) {
    ref->pos.x -= 0.06f;
  }
  if (INPT_DPADDirection(DPAD_RIGHT)) {
    ref->pos.x += 0.06f;
  }
  if (INPT_DPADDirection(DPAD_UP)) {
    ref->pos.z -= 0.06f;
  }
  if (INPT_DPADDirection(DPAD_DOWN)) {
    ref->pos.z += 0.06f;
  }
  if (INPT_Button(BTN_A)) {
    ref->pos.y += 0.06f;
  }
  if (INPT_Button(BTN_B)) {
    if (ref->pos.y >= 1.0f + 0.06f)
      ref->pos.y -= 0.06f;
  }
#endif

  if (INPT_ButtonEx(BTN_Y, BTN_RELEASE)) {
    RNDR_FlipAspect();
  }
}

static void JSONWorld_Render2D(float time) {
  (void)time;
  UI_TextSize(32);
  //UI_DrawStringCentered(320, 440, "Press START to exit!");
  char msg[64];

  float _x = INPT_AnalogF(AXES_X);
  float _y = INPT_AnalogF(AXES_Y);

  float x = _x * SQRT(1 - _y * _y * 0.5f);
  float y = _y * SQRT(1 - _x * _x * 0.5f);

  snprintf(msg, 64, "%3.2f, %3.2f", (double)x, (double)y);
  //UI_DrawStringCentered(300, 180 - 64 - 16, msg);

  UI_TextSize(14);
  UI_TextColorEx(1, 1, 1, 1);
  char mspf[16];
  {
    float current_mspf = ((1000.0f / time) / 1000.0f);
    snprintf(mspf, 16, "%2.02f fps", current_mspf > 60.0f ? 60.0f : current_mspf);
  }
  UI_DrawStringCentered(640 - (4.5 * 16), 420, mspf);

  /* Draw Borders in white */
  dimen_RECT outlines = {0, 0, 640, 2};
  UI_DrawFill(&outlines, 255, 255, 255);
  outlines.y = 480 - 2;
  UI_DrawFill(&outlines, 255, 255, 255);
  outlines.y = 0;
  outlines.w = 2;
  outlines.h = 480;
  UI_DrawFill(&outlines, 255, 255, 255);
  outlines.x = 640 - 2;
  UI_DrawFill(&outlines, 255, 255, 255);
}

static void JSONWorld_Render3D(float time) {
  glPushMatrix();
  glLoadIdentity();

  static float counter = 0.0f;
  counter += (time * 60);

  vec3f direction;
  direction.x = COS(camera.rot.z) * COS(camera.rot.x);
  direction.y = SIN(camera.rot.x);
  direction.z = SIN(camera.rot.z) * COS(camera.rot.x);

  // x: pitch y: roll z: yaw
  glm_vec3_normalize((float *)&direction.x);
  camera_front = direction;

  glm_vec3_sub((float *)&camera.pos.x, (float *)&camera_front.x, (float *)&camera_look.x);

  vec3f cameraUp = (vec3f){0.0f, 1.0f, 0.0f};
  gluLookAt(camera.pos.x, camera.pos.y, camera.pos.z, camera_look.x, camera_look.y, camera_look.z, cameraUp.x, cameraUp.y, cameraUp.z);

  Draw_Objects();
  draw_cube();
  draw_shadow();
  glPopMatrix();
}

static void draw_cube(void) {
  /* Setup VA params */
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glDisable(GL_TEXTURE_2D);
  glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
  glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

  /*Update Object Matrix */
  glPushMatrix();
  glTranslatef(cube.pos.x, cube.pos.y, cube.pos.z);
  //glRotatef(RAD2DEG(objs[i].rot.x), 1, 0, 0);
  //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
  //glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
  //glScalef(cube.scale.x, cube.scale.z, cube.scale.y);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
  glPopMatrix();
}

static void draw_shadow(void) {
  /* Setup VA params */
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  GL_Bind(shadow_tex);

  float opacity = (-0.75f / 4.0f) * (cube.pos.y - 1.0f) + 1.0f;
  float shadow_details[] = {
      /* xyz uv rgba */
      /* Tri 1 */
      -1.0f, 0, -1.0f, 0, 0, 1.0f, 1.0f, 1.0f, opacity,
      -1.0f, 0, 1.0f, 0, 1, 1.0f, 1.0f, 1.0f, opacity,
      1.0f, 0, 1.0f, 1, 1, 1.0f, 1.0f, 1.0f, opacity,
      /* Tri 2 */
      -1.0f, 0, -1.0f, 0, 0, 1.0f, 1.0f, 1.0f, opacity,
      1.0f, 0, 1.0f, 1, 1, 1.0f, 1.0f, 1.0f, opacity,
      1.0f, 0, -1.0f, 1, 0, 1.0f, 1.0f, 1.0f, opacity};

  glColorPointer(4, GL_FLOAT, 9 * sizeof(GLfloat), shadow_details + 5);
  glTexCoordPointer(2, GL_FLOAT, 9 * sizeof(GLfloat), shadow_details + 3);
  glVertexPointer(3, GL_FLOAT, 9 * sizeof(GLfloat), shadow_details);

  /*Update Object Matrix */
  glPushMatrix();
  glTranslatef(cube.pos.x, 0.1f + 0.01f, cube.pos.z);
  //glRotatef(RAD2DEG(objs[i].rot.x), 1, 0, 0);
  //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
  //glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
  //glScalef(cube.scale.x, cube.scale.z, cube.scale.y);
  glScalef(1.25f, 1.0f, 1.25f);
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glPopMatrix();
}

static void Draw_Objects(void) {
  if (objects <= 0) {
    return;
  }
  for (int i = 1; i < objects; i++) {
    if (objs[i].orig_scale == 0.0f) {
      objs[i].orig_scale = 1.0f;
    }
    /* Mesh */
    if (objs[i].tree_obj != NULL) {
      glEnableClientState(GL_VERTEX_ARRAY);
      glDisableClientState(GL_COLOR_ARRAY);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      glEnable(GL_TEXTURE_2D);

      GL_Bind(objs[i].texture);
      OBJ_bind(objs[i].tree_obj);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);
      glRotatef(RAD2DEG(objs[i].rot.x) - 90.0f, 1, 0, 0);
      //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
      glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
      glScalef(objs[i].scale.x / objs[i].orig_scale, objs[i].scale.y / objs[i].orig_scale, objs[i].scale.z / objs[i].orig_scale);
      glDrawArrays(GL_TRIANGLES, 0, objs[i].tree_obj->num_tris);
      glPopMatrix();

    } else {
      /* Cube */

      /* Setup VA params */
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_COLOR_ARRAY);

      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      if (objs[i].texture != NULL) {
        glEnable(GL_TEXTURE_2D);
        GL_Bind(objs[i].texture);
      } else {
        glDisable(GL_TEXTURE_2D);
      }

      glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
      glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);
      //glRotatef(RAD2DEG(objs[i].rot.x), 1, 0, 0);
      //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
      //glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
      glScalef(objs[i].scale.x, objs[i].scale.z, objs[i].scale.y);
      glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
      glPopMatrix();
    }
  }
  //float normalizeAmount = MIN(1 / (spinner_obj.max[0] - spinner_obj.min[0]), 1 / (spinner_obj.max[2] - spinner_obj.min[2]));
  //glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
}