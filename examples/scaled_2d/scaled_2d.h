/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d\scaled_2d.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d
 * Created Date: Monday, April 13th 2020, 5:03:20 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#pragma once
#ifndef SCALED_TEST_H
#define SCALED_TEST_H

#include <common.h>

extern scene scene_scaled_test;

#endif /* SCALED_TEST_H */
